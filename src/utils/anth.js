export function setToken(token) {
  localStorage.setItem('token',token)
}
export function getToken() {
  return localStorage.getItem('token')
}
export function isLogin() {
  return localStorage.getItem('token')?true:false
}
export function deleteToken() {
  localStorage.removeItem('token')
}