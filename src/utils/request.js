import axios from 'axios'

const request = axios.create({
  baseURL: '',
  timeout: 5000
})

request.interceptors.request.use(config => {
  return config
}) 

request.interceptors.response.use(data => {
  return data.data
})

export function get(url,params) {
  return request.get(url,{params}) 
}
export function post(url,data) {
  return request.get(url,{data}) 
}

