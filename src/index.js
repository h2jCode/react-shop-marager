import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router,Route,Switch,Redirect} from 'react-router-dom'
import './index.css';
import App from './App'
import {mainRoutes} from './router'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Router>
    <Switch>
      <Route path="/admin" render={routePages => <App {...routePages}/>} />
      {mainRoutes.map(route => {
        return <Route key={route.path} {...route} />
      })}
      <Redirect to="/admin" from="/"/>
      <Redirect to="/404" />
    </Switch>
  </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
