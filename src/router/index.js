import Dashboard from '../pages/admin/dashboard/Index'
import React from 'react'
import Edit from '../pages/admin/products/Edit'
import ProductList from '../pages/admin/products/ProductList'
import Login from '../pages/Login'
import pageNotFount from '../pages/pageNotFount'
import {AreaChartOutlined,ShoppingFilled} from '@ant-design/icons'

export const mainRoutes = [
  {
    path: "/login",
    component: Login
  },
  {
    path: "/404",
    component: pageNotFount
  }
]

export const adminRoutes = [
  {
    path: '/admin/dashboard',
    component: Dashboard,
    isShow:true,
    title: '看板',
    icon: <AreaChartOutlined />
  },
  {
    path: '/admin/products',
    component: ProductList,
    exact: true,
    isShow:true,
    title: '商品管理',
    icon: <ShoppingFilled />
  },
  {
    path: '/admin/products/edit/:id?',
    component: Edit
  }
]
