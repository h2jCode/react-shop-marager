import React from 'react'
import {Card, Form, Input, Button, message} from 'antd'

function Edit(props) {
  console.log(props)
  let list = JSON.parse(localStorage.getItem('dataList'))
  let index = props.match.params.id

  let goods = list[index]||{}
  let rules=[
    [{required: true, message: '请输入商品名!' }],
    [
      {required: true, type: 'number',transform(value) {
          if(value){
            return value*1;
          }
      },message:'请输入数字！'},
      {
        validator:(_, value) => value>0 ? Promise.resolve() : Promise.reject('库存必须大于0！')
      }
    ],
    [
      {required: true, type: 'number',transform(value) {
          if(value){
            return value*1;
          }
        }, message: '请输入商品价格!',  validator:(_, value) => value>0 ? Promise.resolve() : Promise.reject('库存必须大于0')
      },
    ],
  ]
  const onFinish = values => {
    let list = JSON.parse(localStorage.getItem('dataList'))
    let key = 'loading'
    let newGoods = {
      id: index?goods.id:list.length+1,
      name: values.goodsname,
      num: values.num,
      price:'￥'+values.price
    }
    if(index) list[index] = newGoods
    else list.push(newGoods)
    localStorage.setItem('dataList',JSON.stringify(list))
    message.loading({ content: index?'正在保存修改...':'正在保存商品...', key });
    setTimeout(() => {
      message.success({ content: index?'修改成功!':'添加成功!', key, duration: 2 });
      props.history.push('/admin/products')
    }, 500);
  };
  return (
    <Card>
      <Form onFinish={onFinish}>
        <Form.Item label="商品名"
                  name="goodsname"
                  rules={rules[0]}>
          <Input  placeholder={goods.name?goods.name:"请输入商品名"} value={goods.name||""}/>
        </Form.Item>
        <Form.Item label="库存"
                  name="num"
                  rules={rules[1]}>
          <Input  placeholder={goods.num?goods.num:"请输入商品库存"}/>
        </Form.Item>
        <Form.Item label="价格"
                  name="price"
                  rules={rules[2]}>
                    <Input placeholder={goods.num?goods.num:"输入商品价格"}/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default Edit;
