import React,{useState} from 'react'
import {Card, Table, Button, Popconfirm, message } from 'antd'

function ProductList(props) {
  let data = JSON.parse(localStorage.getItem('dataList'))
  const [dataSource,setDataSource] = useState(data)
  // let dataSource=[]
  function cancel(e) {
    console.log(e);
    message.error('删除失败！');
  }
  let colums = [
    {
      title:'序号',
      key:"id",
      width: 80,
      align: 'center',
      render: (txt,record,index) => index+1
    },
    {
      title:'商品',
      key:"name",
      dataIndex: 'name'
    },
    {
      title:'库存',
      key:"num",
      dataIndex: 'num'
    },
    {
      title:'价格',
      key:"price",
      dataIndex: 'price'
    },
    {
      title: '操作',
      render: (txt,record,index) => {
        return (
          <div>
            <Button type="primary" size="small" onClick={() => {
              props.history.push('/admin/products/edit/'+index)
            }}>编辑</Button>
            <Button type="danger" size="small" style={{marginLeft:"10px"}}>
              <Popconfirm
                title="确定删除吗?"
                onConfirm={() => {
                  dataSource.splice(index,1)
                  let list = [...dataSource]
                  setDataSource(list)
                  localStorage.setItem('dataList',JSON.stringify(list))
                  message.success('删除成功！');
                }}
                onCancel={cancel}
                okText="是"
                cancelText="否"
              >删除
              </Popconfirm>
            </Button>
          </div>
        )
      }
    }
  ]
  
  return (
    <Card title="商品列表" extra={<Button type="primary" onClick={() => props.history.push('/admin/products/edit')}>新增</Button>}>
      <Table rowKey={ e => e.id } columns={colums} bordered dataSource={dataSource} />
    </Card>
  )
}

export default ProductList
