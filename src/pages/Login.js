import React from 'react'
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import {setToken} from '../utils/anth'
import './Login.css'


function login(props){
  const onFinish = values => {
    values.preventDefault()
    let key = 'loading'
    let token = `username:${values.username};password:${values.password}`
    setToken(token)
    message.loading({ content: '正在登录...', key });
    setTimeout(() => {
      message.success({ content: '登录成功!', key, duration: 2 });
      props.history.push('/admin')
    }, 1000);
  };

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <div className="formTitle">用户登录</div>
      <Form.Item
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="输入用户名" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="输入密码"
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>记住账号</Checkbox>
        </Form.Item>

        <a className="login-form-forgot" href="/#">
          忘记密码
        </a>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          登录
        </Button>
        或者 <a href="/#">立即注册!</a>
      </Form.Item>
    </Form>
  );
};

export default login
