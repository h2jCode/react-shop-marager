import React,{useEffect} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom'
import {adminRoutes} from './router'
import 'antd/dist/antd.css'
import Frame from './components/frame/Index'
import './App.css';
import {isLogin} from './utils/anth'
import {dataSource} from './utils/dataSource'

function App() {
  useEffect(() => {
    let list = dataSource()
    localStorage.setItem('dataList',JSON.stringify(list))
  },[])
  return (isLogin()?
    <Frame className="App">
      <Switch>
        {adminRoutes.map(route => <Route key={route.path} path={route.path} exact={route.exact} render={routeProps=>
          <route.component {...routeProps}/>
        } />)}
        <Redirect from="/admin" to={adminRoutes[0].path}/>
        <Redirect to="/404"/>
      </Switch>
    </Frame>:<Redirect to="/login"/>
  );
}

export default App;
