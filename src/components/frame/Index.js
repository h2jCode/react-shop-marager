import React,{useState} from 'react'
import { Layout, Menu, Breadcrumb, Modal, Button, message, Dropdown } from 'antd';
import {adminRoutes} from '../../router/index'
import {withRouter} from 'react-router-dom'
import {ExportOutlined, DownOutlined, UserOutlined, CommentOutlined} from '@ant-design/icons'
import {deleteToken} from '../../utils/anth'
import './index.css'
const { Header, Content, Sider } = Layout;

function Index(props) {
  let routes = adminRoutes.filter(route => route.isShow)
  const [defaultSelectRoute,setDefaultSelectRoute] = useState(routes[0])
  const [visible,setVisible] = useState(false)
  const menu = (
    <Menu onClick={e => {
      if(e.key === "noti"||e.key === "setting") {
        message.success('暂无！')
      }else if(e.key === "logOut") {
        setVisible(true)
      }
      console.log(e)
    }}>
      <Menu.Item key="noti" icon={<CommentOutlined />}>通知中心</Menu.Item>
      <Menu.Item key="setting" icon={<UserOutlined />}>个人中心</Menu.Item>
      <Menu.Item key="logOut" icon={<ExportOutlined />}>退出登录</Menu.Item>
      <Modal
        title="退出登录"
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        footer={[
          <Button key="cancel" onClick={() => {
            message.error('取消退出登录！')
            setVisible(false)}}>
            取消
          </Button>,
          <Button key="ok" type="primary" onClick={() => {
            setVisible(false)
            deleteToken()
            message.success('退出登录成功！')
            props.history.push('/')
          }}>
            确认
          </Button>,
        ]}
      >
        <p>确定退出登录吗?</p>
      </Modal>
    </Menu>
  )
  return (
    <Layout>
      <Header className="header" style={{
        backgroundColor: "#ffffff"
      }}>
        <div className="logo">
          <img src={require('../../assets/img/logo192.png')} alt="logo"/>
        </div>
        <div>
          <Dropdown overlay={menu}>
            <div className="dropdown">
              <span className="dropdownName">Hwj</span>
              <DownOutlined />
            </div>
          </Dropdown>
        </div>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu mode="inline" defaultSelectedKeys={defaultSelectRoute.path} defaultOpenKeys={['sub1']} style={{ height: '100%', borderRight: 0 }}>
            {routes.map(route => {
              return <Menu.Item key={route.path} icon={route.icon} onClick={e => {
                setDefaultSelectRoute(route)
                props.history.push(e.key)
              }}>{route.title}</Menu.Item>
            })}
            
          </Menu>
        </Sider>
        <Layout style={{ padding: '0 24px 24px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>{defaultSelectRoute.title}</Breadcrumb.Item>
          </Breadcrumb>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {props.children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  )
}

export default withRouter(Index)
